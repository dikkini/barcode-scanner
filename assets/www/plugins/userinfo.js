cordova.define("cordova/plugins/userinfo",
    function(require, exports, module) {
        var exec = require("cordova/exec");
        var UserInfo = function() {};

        //-------------------------------------------------------------------
        UserInfo.prototype.getInfo = function(successCallback, errorCallback) {
            if (errorCallback == null) { errorCallback = function() {}}

            if (typeof errorCallback != "function")  {
                console.log("UserInfo.getInfo failure: failure parameter not a function");
                return
            }

            if (typeof successCallback != "function") {
                console.log("UserInfo.getInfo failure: success callback parameter must be a function");
                return
            }

            exec(successCallback, errorCallback, 'UserInfo', 'getInfo', []);
        };
        var userInfo = new UserInfo();
        module.exports = userInfo;

    });

if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.userInfo) {
    window.plugins.userInfo = cordova.require("cordova/plugins/userinfo");
}