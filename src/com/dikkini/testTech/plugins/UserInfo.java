package com.dikkini.testTech.plugins;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

public class UserInfo extends CordovaPlugin {

    public UserInfo() {}

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        TelephonyManager tMgr = (TelephonyManager) cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        JSONObject result = new JSONObject();
        try {
            result.put("phone", tMgr.getLine1Number());
            result.put("imei", tMgr.getDeviceId());
            result.put("email", getEmailAddress());
            result.put("simNumber", tMgr.getSimSerialNumber());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        callbackContext.success(result);
        return false;
    }


    private String getEmailAddress() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(cordova.getActivity()).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }
        return null;
    }
}
